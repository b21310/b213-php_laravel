<?php

function getLetterGrade($grade){
    if($grade >= 98 && $grade <=100){
        console.log(`${$grade} is equivalent to A+`)
    }
    else if($grade >= 95 && $grade <=97){
        console.log(`${$grade} is equivalent to A`)
    }
    else if($grade >= 92 && $grade <=94){
        console.log(`${$grade} is equivalent to A-`)
    }
    else if($grade >= 89 && $grade <=91){
        console.log(`${$grade} is equivalent to B+`)
    }
    else if($grade >= 86 && $grade <=88){
        console.log(`${$grade} is equivalent to B`)
    }
    else if($grade >= 83 && $grade <=85){
        console.log(`${$grade} is equivalent to B-`)
    }
    else if($grade >= 80 && $grade <=82){
        console.log(`${$grade} is equivalent to C+`)
    }
    else if($grade >= 77 && $grade <=79){
        console.log(`${$grade} is equivalent to C`)
    }
    else if($grade >= 75 && $grade <=76){
        console.log(`${$grade} is equivalent to C+`)
    }
    else {
                console.log(`${$grade} is equivalent to D`)
    }
}